from selenium import webdriver
from behave import *
from Features.classes.Home import Avenue_Home
from Features.classes.Fuctionality import Mytasks
import time
from dateutil.parser import *

@given('I am on qa-test-aavanue home page')
def step_impl(context):
    context.driver =webdriver.Chrome()
    context.home = Avenue_Home(context.driver)
    context.home.goto_home()
    time.sleep(6)

@when('I click on login to redirect to login page')
def step_impl(context):
     context.home.click_on_login()
     time.sleep(5)

@then('Enter valid user credentials')
def step_impl(context):
     context.home.add_login_details('vijayreddy0323@gmail.com', 'Vijay@123')
     time.sleep(3)

@then('It will show todo home page')
def step_impl(context):
    context.home = Mytasks(context.driver)
    context.home.add_tasks()


@then('Create tasks for todo app')
def step_impl(context):
    current_time = str(int(time.time()))
    context.home.add_new_task('xyz' + current_time)

@then('add new sub tasks to existing tasks')
def step_impl(context):
    context.home.add_sub_tasks('Finish tasks asap','08/08/2017' )





