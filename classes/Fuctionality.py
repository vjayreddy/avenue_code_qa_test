import time
from datetime import datetime
class Mytasks():

    def __init__(self,driver):
        self.driver =driver

    def add_tasks(self):
        self.driver.find_element_by_class_name('btn-success').click()

    def add_new_task(self,Script):
        task = self.driver.find_element_by_id('new_task')
        task.send_keys(Script)
        self.driver.find_element_by_class_name('glyphicon-plus').click()
        time.sleep(5)

    def add_sub_tasks(self,New_task,date):
        NEw1 = self.driver.find_element_by_xpath('/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[1]/td[4]/button').click()
        sub_task= self.driver.find_element_by_id('new_sub_task')
        sub_task.send_keys(New_task)
        time.sleep(5)
        datetime = self.driver.find_element_by_name('due_date')
        datetime.clear()
        datetime.send_keys(date)
        time.sleep(5)
        self.driver.find_element_by_id('add-subtask').click()
        time.sleep(5)

