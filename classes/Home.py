

class Avenue_Home():

    def __init__(self,driver):
        self.driver =driver

    def goto_home(self):
        self.driver.get('http://qa-test.avenuecode.com/')
        title = self.driver.title
        assert (title == 'ToDo Rails and Angular')


    def click_on_login(self):
        login_link = self.driver.find_element_by_link_text('Sign In')
        login_link.click()

    def add_login_details(self,Email,PWD):
        email = self.driver.find_element_by_id('user_email')
        email.send_keys(Email)
        pswd = self.driver.find_element_by_name('user[password]')
        pswd.send_keys(PWD)
        Signin = self.driver.find_element_by_name('commit').click()


